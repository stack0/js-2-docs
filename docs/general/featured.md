# Featured Images

Jetstream2 has a limited set of featured operating system images. These are images that the Jestream2 team curates and maintains.

A key difference between Jetstream1 and Jetstream2 is that Jetstream2 does not provide application-specific featured images. We maintain a [software collection](software.md) that is available on every virtual machine at boot. Jetstream2 offers software via [Lmod Modules](https://lmod.readthedocs.io/en/latest/){target=_blank}.

At this time, the featured images are:

* Ubuntu 22.04
* Ubuntu 20.04 *(will retire soon -- will NOT support GPU operations)*
* Rocky Linux 8 / 9

Retired images:

* AlmaLinux 8 / 9 (due to lack of use and functional equivalence to Rocky Linux)

All featured images are named ***Featured-yyyyyyyy*** (e.g. Featured-Ubuntu22) on Jetstream2.

These featured images will evolve over time. As distributions leave support (e.g Ubuntu 20 will end support later this year), we will replace them with newer, supported versions. NVIDIA drivers will be present on all\* featured images so any of the featured images will work with Jetstream2 GPUs.

!!! Note "\*Ubuntu 20.04 and GPUs"

     Due to issues with the NVIDIA GRID driver, we have discontinued support for GPUs using Ubuntu 20. We will be removing Ubuntu 20 from the featured images once we have a stable Ubuntu 24 build available.

Our goal is to maintain a minimum of featured images but keep them updated via automated pipeline on a weekly basis.

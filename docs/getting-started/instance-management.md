# Instance Management And SU Conservation

## SU Consumption

As long as an instance is running, even if you are not actively using it, it is consuming Service Units (SUs). You can see this consumption in the top right of the Instance Details Page.

![burn rate highlighted on instance details page](../images/exo-burn-rate.png)

If your instance is being used for a server or other service that requires it to be online 24/7, then leaving it running is the best course of action. However, if you use your instance as a virtual workstation, there are some actions you can do to save on usage consumption.

## Shelving

The most efficient way to conserve your SUs is to shelve your instance. Shelving an instance automatically creates an image of your instance that will be used to create a new, albeit identical, instance at a later time when you unshelve the instance.

Shelving and unshelving can take several minutes to complete, but the SU consumption goes to Zero while the instance is shelved. Let's try this out.

**Shelve your instance by clicking on the Actions bar, then selecting Shelve.**

![Select Actions, then Shelve](../images/exo-actions-shelve.png)

You will see the status sticker change to `Shelving`. After several minutes, it will change to `Shelved`, at which point you should see the Burn rate change to 0.00 SUs/hour.

![Instance status is shelved, and burn rate is zero](../images/exo-shelved-burnrate.png)

***Note***: It is good practice to detach your volumes before shelving an instance. If you have been following along with this tutorial, we did that in a previous section, [Volume Management - Removing the Volume](volumes.md#removing-the-volume).
{: .note}

## Unshelving

When you want to use your instance again, you can unshelve the instance to bring it back up.

**Unshelve your instance by clicking on the Actions bar, then selecting Unshelve.**

After several minutes the status should be set back to Ready and you should see the burn rate change back to 2.00 SUs/hour.

## Other Actions

This tutorial only goes through shelving and unshelving actions as these are the most important actions for conserving your SUs. If you would like to learn more about the other Actions listed here, please see [Instance Management Actions](../general/instancemgt.md).

## Next
Finally, we move on to the last topic for the tutorial, [Snapshots and Images](snapshots.md).